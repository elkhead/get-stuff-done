import React from 'react';
import './Utility.css';

const Wrapper = ({ children }) => {
    return (
        <div className='simple-wrapper'>
            { children }
        </div>
    )
}

export default Wrapper