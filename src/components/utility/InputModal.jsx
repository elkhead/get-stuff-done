import React, { useEffect, useState } from "react";
import "./Utility.css";

const InputModal = ({ handleSubmit, handleClose, editItem, title }) => {
  const [dueDate, setDueDate] = useState("");
  const [task, setTask] = useState("");
  const [description, setDescription] = useState("");
  const [isChecked, setIsChecked] = useState(false);

  const submitTask = (e) => {
    e.preventDefault();
    handleSubmit(dueDate, task, description, isChecked);
    setTask("");
    setDescription("");
  };

  useEffect(() => {
    if (Object.keys(editItem).length !== 0) {
      setTask(editItem.task);
      setDueDate(editItem.date);
      setDescription(editItem.description);
    }
  }, [editItem]);

  const addCheckbox = () => {
    return (
      <div className="log-another">
        <input
          className="log-another-checkbox"
          type="checkbox"
          name="log-another-check"
          checked={isChecked ? "checked" : ""}
          onChange={() => setIsChecked((isChecked) => !isChecked)}
        />
        <label onClick={() => setIsChecked((isChecked) => !isChecked)}>
          Add another
        </label>
      </div>
    );
  };

  return (
    <div className="modal-on">
      <form className="modal" onSubmit={(e) => submitTask(e)}>
        <div className="close-x">
          <span className="close" onClick={() => handleClose()}>
            &times;
          </span>
        </div>
        <div className="modal-body">
          <h3>{title}</h3>
          <input
            style={{ color: "white" }}
            type="date"
            className="input"
            placeholder="dd-mm-yy"
            required
            value={dueDate}
            onChange={(e) => setDueDate(e.target.value)}
          />
          <input
            type="text"
            className="input"
            placeholder="Task"
            required
            value={task}
            onChange={(e) => setTask(e.target.value)}
          />
          <textarea
            name="description"
            className="description-box input"
            placeholder="Description"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
          />
          {Object.keys(editItem).length === 0 && addCheckbox()}
          <span className="btn-container">
            <button className="modal-btn btn" type="submit">
              Submit
            </button>
          </span>
        </div>
      </form>
    </div>
  );
};

export default InputModal;
