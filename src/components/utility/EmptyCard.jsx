import React from "react";
import "./Utility.css";

const EmptyCard = ({ children, bc, tc, cw, ch, br, rb, lb, tb, bb, cp }) => {
  const cardColor = bc === undefined ? "white" : bc;
  const textColor = tc === undefined ? "black" : tc;
  const cardWidth = cw === undefined ? "100%" : cw;
  const cardHeight = ch === undefined ? "" : ch;
  const cardRadius = br === undefined ? '0px' : br;
  const rightBorder = rb === undefined ? '1px solid #c5c4c444' : rb;
  const leftBorder = lb === undefined ? '1px solid #c5c4c444' : lb;
  const topBorder = tb === undefined ? '1px solid #c5c4c444' : tb;
  const bottomBorder = bb === undefined ? '1px solid #c5c4c444' : bb;
  const cardPadding = cp === undefined ? '0px' : cp;

  return (
    <div
      className="empty-card"
      style={{
        backgroundColor: cardColor,
        color: textColor,
        flex: cardWidth,
        height: cardHeight,
        borderRadius: cardRadius,
        borderLeft: leftBorder,
        borderRight: rightBorder,
        borderTop: topBorder,
        borderBottom: bottomBorder,
        padding: cardPadding
      }}
    >
      {children}
    </div>
  );
};

export default EmptyCard;
