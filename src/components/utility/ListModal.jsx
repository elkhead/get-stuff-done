import React from "react";
import Task from "../tasks/Task";

const ListModal = ({ listItems, handleClose }) => {
  const getTasks = () => {
    return (
      <ul>
        {listItems.map((item, index) => (
          <li key={index}>
            <Task task={item} isExpandable={true}/>
          </li>
        ))}
      </ul>
    );
  };

  return (
    <div className="modal-on">
      <div className="xx">
        <div className="test">
          <div className="closing">
            <span className="close" onClick={() => handleClose()}>
              &times;
            </span>
          </div>
          <h3 style={{ lineHeight: '0'}}>Tasks</h3>
        </div>
        <div className="list-modal">
        {listItems.length === 0 ? (
          <div className="no-tasks-container">
            <p style={{ fontSize: "25px", color: "grey" }}>
              No tasks on this day
            </p>
          </div>
        ) : (
          getTasks()
        )}
          </div>
      </div>
    </div>
  );
};

export default ListModal;
