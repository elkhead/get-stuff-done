import React, { useEffect, useState, useContext } from "react";
import { TasksContext } from "../../contexts/TasksContext";
import EmptyCard from "../utility/EmptyCard";
import "./Tasks.css";

const Task = ({ task, isEditable, hanleEditTask, isExpandable }) => {
  const borderRadius = "5px";
  const { stateVariables, actionFunctions } = useContext(TasksContext);
  const { tasks } = stateVariables;
  const { setTasks } = actionFunctions;
  const [taskBackground, setTaskBackground] = useState("black");
  const [textColor, setTextColor] = useState("white");
  const [isExpanded, setIsExpanded] = useState(false);
  const { date } = task;
  const today = new Date();
  let dueDate = new Date(date);

  const isPastDue = () => {
    dueDate = new Date(
      dueDate.getUTCFullYear(),
      dueDate.getUTCMonth(),
      dueDate.getUTCDate()
    );
    return today > dueDate;
  };

  const formatDueDate = () => {
    const utcDate = dueDate.toUTCString();
    return utcDate.slice(0, 3) + " " + utcDate.slice(8, 11) + " " + utcDate.slice(5, 7);
  }

  // ToDo: clean this shit up
  const decorateTask = () => {
    if (task.isCompleted) {
      setTaskBackground("black");
      setTextColor("white");
      return;
    } else if (isPastDue()) {
      setTaskBackground("black");
      setTextColor("white");
      return;
    }

    setTaskBackground("black");
    setTextColor("white");
    return;
  };

  const handleComplete = () => {
    const ind = tasks.findIndex((currentTask) => currentTask.id === task.id);
    tasks[ind].isCompleted = !tasks[ind].isCompleted;
    // task.isCompleted ? setCheckBoxColor('#00a545') : setCheckBoxColor('white');
    setTasks(tasks);
    localStorage.setItem("tasks", JSON.stringify(tasks));
  };

  const deleteTask = () => {
    const tasksAfterDeletion = tasks.filter(
      (currentTask) => task.id !== currentTask.id
    );
    setTasks(tasksAfterDeletion);
    localStorage.setItem("tasks", JSON.stringify(tasksAfterDeletion));
  };

  const hanleExpandTask = () => {
    setIsExpanded(!isExpanded);
  };

  useEffect(() => {
    decorateTask();
  });

  const formatTask = () => {
    const taskWidth =
      (
        100 /
        (Object.keys(task).length - 1 + (isEditable || isExpandable ? 1 : 0))
      ).toString() + "%";
    return (
      <ul>
        {Object.keys(task).map((item, index) => {
          const dateClass = (item === 'date' && isPastDue() && !task.isCompleted) ? ' past-due' : '';
          const itemClass = "task-item " + item + dateClass;
          return (
            item !== "id" &&
            item !== "isCompleted" && 
            item !== "description" && (
              <li
                key={index}
                className={itemClass}
                style={{ width: taskWidth }}
              >
                {task[item]}
              </li>
            )
          );
        })}
        {isEditable && (
          <li className="task-item edit" style={{ width: taskWidth }}>
            <button className="edit-btn" onClick={() => hanleEditTask(task)}>
              Edit
            </button>
          </li>
        )}
        {isExpandable && (
          <li className="task-item edit" style={{ width: taskWidth }}>
            <button className="edit-btn" onClick={() => hanleExpandTask(task)}>
              Details
            </button>
          </li>
        )}
        <li className="task-item done" style={{ width: taskWidth }}>
          <input
            className="checkbox"
            type="checkbox"
            checked={task.isCompleted ? "checked" : ""}
            onChange={() => handleComplete()}
          />
        </li>
        <span className="delete-x" onClick={() => deleteTask()}>
          &times;
        </span>
      </ul>
    );
  };

  return (
    <div className="unpadded-left-right">
      <EmptyCard bc={taskBackground} tc={textColor} br={borderRadius}>
        {formatTask()}
        {isExpanded && (
          <EmptyCard
            bc={taskBackground}
            rb={"hidden"}
            lb={"hidden"}
            bb={"hidden"}
            tc={textColor}
            cp={"0px 10px 10px 10px"}
          >
            <div
              style={{
                fontWeight: "bold",
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <span style={{ paddingRight: '10px'}}>
                <p style={{ marginBottom: '0'}}>{task.task}</p>
                <p style={{ fontSize: "12px", color: "grey", marginTop: '5px' }}>{formatDueDate()}</p>
              </span>
              {task.isCompleted ? (
                <p style={{ color: "green" }}>completed</p>
              ) : (
                <p style={{ color: "red" }}>incomplete</p>
              )}
            </div>
            <p style={{ fontSize: "13px", margin: "0px" }}>
              {task.description}
            </p>
          </EmptyCard>
        )}
      </EmptyCard>
    </div>
  );
};

export default Task;
