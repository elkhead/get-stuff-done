import React, { useContext, useState, useEffect } from "react";
import { TasksContext } from "../../contexts/TasksContext";
import InputModal from "../utility/InputModal";
import "./Tasks.css";
import Task from "./Task";

const TasksList = ({ isEditable }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [editItem, setEditItem] = useState({});
  const [isEdit, setIsEdit] = useState(false);
  const [title, setTitle] = useState("");
  const { stateVariables, actionFunctions } = useContext(TasksContext);
  const [completed, setCompleted] = useState([]);
  const [incomplete, setIncomplete] = useState([]);
  const [completedTasksCount, setCompletedTasksCount] = useState();
  const [incompleteTasksCount, setIncompleteTasksCount] = useState();
  const { tasks } = stateVariables;
  const { setTasks } = actionFunctions;

  const handleNewTask = () => {
    setTitle("Add New Task");
    setIsModalOpen(!isModalOpen);
  };

  const addNewTask = (date, task, description) => {
    const newId =
      tasks.length === 0 ? 0 : Math.max(...tasks.map((task) => task.id)) + 1;
    const updatedTasks = [
      {
        id: newId,
        task: task,
        description: description,
        date: date,
        isCompleted: false,
      },
      ...tasks,
    ];

    return updatedTasks;
  };

  // found this shit online. it works and I don't know how
  const sortByDate = (unsortedTasks) => {
    const sorter = (a, b) => {
      return new Date(a.date).getTime() - new Date(b.date).getTime();
    };
    unsortedTasks.sort(sorter);
  };

  const updateTask = (date, task, description) => {
    const modifiedTasks = [...tasks];
    const ind = modifiedTasks.findIndex(
      (currentTask) => currentTask.id === editItem.id
    );
    modifiedTasks[ind].date = date;
    modifiedTasks[ind].task = task;
    modifiedTasks[ind].description = description;
    return modifiedTasks;
  };

  const handleSubmit = (date, task, description, isChecked) => {
    !isChecked && setIsModalOpen(!isModalOpen);
    const updatedTasks = isEdit
      ? updateTask(date, task, description)
      : addNewTask(date, task, description);
    sortByDate(updatedTasks);
    updatedTasks.forEach((element) => {
      if (!Object.keys(element).includes("description")) {
        element["description"] = "";
      }
    });
    localStorage.setItem("tasks", JSON.stringify(updatedTasks));
    setTasks(updatedTasks);
    setEditItem({});
    isEdit && setIsEdit((isEdit) => !isEdit);
  };

  const handleClose = () => {
    isEdit && setIsEdit((isEdit) => !isEdit);
    setIsModalOpen(!isModalOpen);
    setEditItem({});
  };

  const hanleEditTask = (task) => {
    !isEdit && setIsEdit((isEdit) => !isEdit);
    setEditItem(task);
    setTitle("Edit Task");
    setIsModalOpen(!isModalOpen);
  };

  const clearCompletedTasks = () => {
    const notCompleted = tasks.filter((task) => !task.isCompleted);
    localStorage.setItem("tasks", JSON.stringify(notCompleted));
    setTasks(notCompleted);
  };

  const completedTasks = () => tasks.filter((task) => task.isCompleted);
  const incompleteTasks = () => tasks.filter((task) => !task.isCompleted);

  // fix dependency array
  useEffect(() => {
    setCompletedTasksCount(completedTasks().length);
    setIncompleteTasksCount(incompleteTasks().length);
    setCompleted(completedTasks());
    setIncomplete(incompleteTasks());
  }, [JSON.stringify(tasks)]);

  const handleSearch = (searchInput, isCompleted) => {
    const tasksArrayToSearch = isCompleted ? completedTasks() : incompleteTasks();
    console.log(tasksArrayToSearch)
    const matchingTasks = tasksArrayToSearch.filter((task) =>
      task.task.toLowerCase().includes(searchInput.toLowerCase())
    );

    console.log(matchingTasks)
    if (isCompleted) {
      setCompleted(matchingTasks);
    } else {
      setIncomplete(matchingTasks)
    }
    // isCompleted ? setCompleted(matchingTasks) : setIncomplete(matchingTasks)
  };

  const header = () => {
    return (
      <div className="header">
        <h3>Tasks</h3>
        <button className="btn" onClick={() => handleNewTask()}>
          New Task
        </button>
      </div>
    );
  };

  const getTasks = () => {
    return (
      <ul className="tasks-container">
        <div className="search-container">
          <input
            className="search-input"
            type="text"
            placeholder="Search for tasks..."
            onChange={(e) => handleSearch(e.target.value, false)}
          />
        </div>
        {tasks.length === 0 || tasks.length === completed.length ? (
          <div className="no-tasks-container">
            <p>No tasks to complete</p>
          </div>
        ) : (
          incomplete.map((item, index) => (
            <li key={index}>
              <Task
                task={item}
                isEditable={isEditable}
                hanleEditTask={hanleEditTask}
              />
            </li>
          ))
        )}
      </ul>
    );
  };

  const getCompletedTasks = () => {
    return (
      <div>
        <div className="header">
          <h3>Completed Tasks</h3>
          <button className="btn" onClick={() => clearCompletedTasks()}>
            Clear
          </button>
        </div>
        <div className="tasks-count-circle">
          <span className="tasks-count" onClick={() => {}}>
            {completedTasksCount}
          </span>
        </div>
        <ul className="tasks-container">
          <div className="search-container">
            <input
              className="search-input"
              type="text"
              placeholder="Search for completed tasks..."
              onChange={(e) => handleSearch(e.target.value, true)}
            />
          </div>
          {completed.length === 0 ? (
            <div className="no-tasks-container">
              <p>No completed tasks</p>
            </div>
          ) : (
            completed.map((item, index) => (
              <li key={index}>
                <Task task={item} />
              </li>
            ))
          )}
        </ul>
      </div>
    );
  };

  return (
    <div className="padded-right">
      {isModalOpen && (
        <InputModal
          handleSubmit={handleSubmit}
          handleClose={handleClose}
          editItem={editItem}
          title={title}
        />
      )}
      <div>
        {header()}
        <div className="tasks-count-circle">
          <span className="tasks-count">{incompleteTasksCount}</span>
        </div>
        {getTasks()}
      </div>
      {getCompletedTasks()}
    </div>
  );
};

export default TasksList;
