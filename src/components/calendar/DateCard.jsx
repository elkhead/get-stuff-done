import React, { useContext, useEffect, useState } from "react";
import { TasksContext } from "../../contexts/TasksContext";

const DateCard = ({ children, tc, handleDateClick, date }) => {
  const [newTasksForDate, setNewTasksForDate] = useState([]);
  const textColor = tc === undefined ? "white" : tc;
  const { stateVariables } = useContext(TasksContext);
  const { tasks } = stateVariables;
  const [sortedByCompleteion, setSortedByCompletion] = useState([]);
  const today = new Date();
  
  const isItToday = () => {
    if (today.getFullYear() !== date.getFullYear()) {
      return false;
    }

    if (today.getMonth() !== date.getMonth()) {
      return false;
    }

    if (today.getDate() !== date.getDate()) {
      return false;
    }

    return true;
  };

  // probably a better way to do this
  const sortedTaksByCompletion = tasksForDate => {
    const completedTasks = tasksForDate.filter((task) => task.isCompleted);
    const incompletedTasks = tasksForDate.filter((task) => !task.isCompleted);
    return [...incompletedTasks, ...completedTasks];
    
  }

  const dashedDateFormat = date
    .toISOString("en-US", {
      year: "numeric",
      day: "2-digit",
      month: "2-digit",
    })
    .replaceAll("/", "-")
    .substring(0, 10);

  useEffect(() => {
    const newTasks = tasks.filter((task) => task.date === dashedDateFormat);
    setNewTasksForDate(sortedTaksByCompletion(newTasks));
  }, [date, tasks]);

  return (
    <div
      className="date-card"
      onClick={() => handleDateClick(dashedDateFormat)}
      style={{ color: textColor }}
    >
      <div>
        <p className={isItToday() ? "today" : "not-today"}>{children}</p>
        {newTasksForDate.length > 0 && (
          <div style={{ margin: "auto" }}>
            {newTasksForDate.slice(0, 3).map((task, index) => {
              const taskColor = (task.isCompleted ? "black" : "blue");
              return (
                <p
                  key={index}
                  className="date-card-task"
                  style={{ backgroundColor: taskColor }}
                >
                  {task.task}
                </p>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
};

export default DateCard;
