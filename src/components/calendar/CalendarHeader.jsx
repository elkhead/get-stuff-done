import React, { useContext } from "react";
import { CalendarContext } from "../../contexts/CalendarContext";

const CalendarHeader = () => {
  const { date, setDate } = useContext(CalendarContext);

  const getMonthAndYear = () => {
    return date.toLocaleString("en-US", { month: "long", year: "numeric" });
  };

  const handleNext = () => {
    const nextMonth = new Date(date.getFullYear(), date.getMonth() + 1);
    setDate(nextMonth);
  };

  const handlePrevious = () => {
    const previousMonth = new Date(date.getFullYear(), date.getMonth() - 1);
    setDate(previousMonth);
  };

  return (
    <ul className="cal-header">
      <li className="header-item">
        <button className="btn" onClick={() => handlePrevious()}>
          Previous
        </button>
      </li>
      <li className="header-item">
        <h3>{getMonthAndYear()}</h3>
      </li>
      <li className="header-item">
        <button className="btn" onClick={() => handleNext()}>
          Next
        </button>
      </li>
    </ul>
  );
};

export default CalendarHeader;
