import React, { useContext, useEffect, useState } from "react";
import { CalendarContext } from "../../contexts/CalendarContext";
import { TasksContext } from "../../contexts/TasksContext";
import ListModal from "../utility/ListModal";
import DateCard from "./DateCard";

const DateCards = () => {
  const [tasksForDate, setTasksForDate] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedDate, setSelectedDate] = useState('');
  const { stateVariables } = useContext(TasksContext);
  const { tasks } = stateVariables;
  const { date } = useContext(CalendarContext);
  const year = date.getFullYear();
  const month = date.getMonth();

  const numberOfDaysThisMonth = new Date(year, month + 1, 0).getDate();
  const currentMonthDates = [...Array(numberOfDaysThisMonth).keys()].map((i) => i + 1);

  const previousMonth = new Date(year, month, 0);
  const previousMonthNumberOfDays = previousMonth.getDate();
  const lastDayOfPreviousMonth = previousMonth.getDay();

  const numberOfDaysLastDayDiff = previousMonthNumberOfDays - lastDayOfPreviousMonth;
  const lastCalendarDate = 42 - (currentMonthDates.length + (lastDayOfPreviousMonth + 1));

  let lastDaysOfPreviousMonth = [...Array(previousMonthNumberOfDays - numberOfDaysLastDayDiff + 1)
    .keys()].map((i) => i + numberOfDaysLastDayDiff);

  lastDaysOfPreviousMonth = lastDaysOfPreviousMonth.length === 7 ? [] : lastDaysOfPreviousMonth;
  const lastDateOnCalendar = lastDaysOfPreviousMonth.length === 0 ? lastCalendarDate + 7 : lastCalendarDate < 0 ? 0 : lastCalendarDate;
  const newMonthDates = [...Array(lastDateOnCalendar).keys()].map((i) => i + 1);

  const updateTasksForDate = calendarDate => {
    return tasks.filter((task) => task.date === calendarDate);
  }

  const handleDateClick = calendarDate => {
    setSelectedDate(calendarDate);
    setTasksForDate(updateTasksForDate(calendarDate));
    setIsModalOpen(!isModalOpen);
  }
  
  useEffect(() => {
    setTasksForDate(updateTasksForDate(selectedDate));
  }, [tasks]);

  const closeModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const getDates = () => {
    return [
      ...[
        ...lastDaysOfPreviousMonth.map((date, index) => (
          <DateCard
            tc={'grey'}
            key={-(index + 1)}
            handleDateClick={handleDateClick}
            date={new Date(year, month - 1, date)}>
                {date}
          </DateCard>
        )),
        ...currentMonthDates.map((date, index) => (
          <DateCard
            key={index}
            handleDateClick={handleDateClick}
            date={new Date(year, month, date)}>
                  {date}
          </DateCard>
        )),
      ],

      ...newMonthDates.map((date, index) => (
        <DateCard
          tc={'grey'}
          key={index + 35}
          handleDateClick={handleDateClick}
          date={new Date(year, month + 1, date)}>
            {date}
        </DateCard>
      )),
    ];
  };

  return <div className="date-cards-container">
    {isModalOpen && (
        <ListModal
          listItems={tasksForDate}
          handleClose={closeModal}
        />
      )}
      {getDates()}
    </div>;
};

export default DateCards;
