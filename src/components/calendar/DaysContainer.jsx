import React from 'react'

const DaysContainer = () => {
    const days = [
        { id: 'sun', name: 'Sunday' },
        { id: 'mon', name: 'Monday' },
        { id: 'tue', name: 'Tuesday' },
        { id: 'wed', name: 'Wednesday' },
        { id: 'thur', name: 'Thursday' },
        { id: 'fri', name: 'Friday' },
        { id: 'sat', name: 'Saturday' }
    ];

  return (
    <ul className='days-container'>{days.map((day) => 
        <li className="calendar-day" key={day.id}>{ day.name }</li>
    )}</ul>
  )
}

export default DaysContainer