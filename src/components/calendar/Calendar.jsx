import React from "react";
import EmptyCard from "../utility/EmptyCard";
import "./Calendar.css";
import CalendarHeader from "./CalendarHeader";
import DaysContainer from "./DaysContainer";
import DateCards from "./DateCards";
import TasksList from "../tasks/TasksList";

const Calendar = () => {
  return (
    <div className="container">
      <div className="calendar-tl-container">
        <TasksList isEditable={true}/>
      </div>
      <div className="calendar-body-container">
        <CalendarHeader />
        <EmptyCard bc={'black'} bb={'hidden'}>
          <DaysContainer />
        </EmptyCard>
        <EmptyCard bc={'black'} tb={'hidden'} rb={'hidden'}>
          <DateCards />
        </EmptyCard>
      </div>
    </div>
  );
};

export default Calendar;
