import { actionTypes } from "./actions/ActionTypes";

const AppReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.SET_TASKS: {
      return { ...state, tasks: action.value };
    }

    case actionTypes.SET_DATE: {
      return { ...state, date: action.value }
    }

    default:
      return state;
  }
};

export default AppReducer;