import { createContext, useReducer } from "react";
import InitialState from "../data/InitialState";
import { actions } from "./actions/Actions";
import AppReducer from "./AppReducer";

export const CalendarContext = createContext();

export const CalendarProvider = ({ children }) => {
    const [state, dispatch] = useReducer(AppReducer, InitialState());

    const date = state.date;
    const setDate = (newVal) => dispatch(actions.setDate(newVal))

    const store = {
        date,
        setDate
    }

    return <CalendarContext.Provider value={ store }>
        { children }
    </CalendarContext.Provider>
}