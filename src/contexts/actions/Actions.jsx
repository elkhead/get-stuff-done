import { actionTypes } from "./ActionTypes";

const setTasks = (newVal) => ({
    type: actionTypes.SET_TASKS,
    value: newVal,
});

const setDate = (newVal) => ({
    type: actionTypes.SET_DATE,
    value: newVal
});

export const actions = {
    setTasks,
    setDate,
}