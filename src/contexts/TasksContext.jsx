import { createContext, useReducer, useState } from "react";
import InitialState from "../data/InitialState";
import { actions } from "./actions/Actions";
import AppReducer from "./AppReducer";

export const TasksContext = createContext();

export const TasksProvider = ({ children }) => {
    const [state, dispatch] = useReducer(AppReducer, InitialState());

    const stateVariables = {
        tasks: state.tasks,
    }

    const actionFunctions = {
        setTasks: (newVal) => dispatch(actions.setTasks(newVal))
    }

    const store = {
        stateVariables,
        actionFunctions,
    };
    
    return <TasksContext.Provider value={ store }>
        { children }
    </TasksContext.Provider>
};