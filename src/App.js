import "./App.css";
import Calendar from "./components/calendar/Calendar";
import Wrapper from "./components/utility/Wrapper";
import { CalendarProvider } from "./contexts/CalendarContext";
import { TasksProvider } from "./contexts/TasksContext";

function App() {
  return (
    <TasksProvider>
      <CalendarProvider>
        <Wrapper>
          <Calendar />
        </Wrapper>
      </CalendarProvider>
    </TasksProvider>
  );
}

export default App;
