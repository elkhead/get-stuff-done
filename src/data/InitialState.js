import useLocalStorage from "../hooks/useLocalStorage";

const InitialState = () => {
  const cachedTasks = JSON.parse(localStorage.getItem('tasks'));
  const initialTasks = [
    // { id: 14, task: "Do Groceries", description: "this is just a test buddy", date: "2022-05-20", isCompleted: false },
    // { id: 13, task: "Start reading", description: "this is just a test buddy", date: "2022-05-20", isCompleted: false },
    // { id: 12, task: "Kill a whale", description: "this is just a test buddy", date: "2022-05-20", isCompleted: false },
    // { id: 11, task: "Pray", description: "this is just a test buddy", date: "2022-05-20", isCompleted: false },
    // { id: 10, task: "Eat something", description: "this is just a test buddy", date: "2022-05-20", isCompleted: false },
    // { id: 9, task: "Make music", description: "this is just a test buddy", date: "2022-05-20", isCompleted: false },
    // { id: 8, task: "Make music", description: "this is just a test buddy", date: "2022-04-20", isCompleted: false },
    // { id: 7, task: "English class", description: "this is just a test buddy", date: "2022-04-20", isCompleted: false },
    // { id: 6, task: "Write something", description: "this is just a test buddy", date: "2022-04-20", isCompleted: false },
    // { id: 5, task: "Smoke Weed", description: "this is just a test buddy", date: "2022-03-30", isCompleted: false },
    // { id: 4, task: "clean room again", description: "this is just a test buddy", date: "2022-03-30", isCompleted: false },
    // { id: 3, task: "clean room", description: "this is just a test buddy", date: "2022-03-30", isCompleted: false },
    // { id: 2, task: "Do the dishes", description: "this is just a test buddy", date: "2022-03-30", isCompleted: false },
    // { id: 1, task: "Burn the house", description: "this is just a test buddy", date: "2022-03-30", isCompleted: false },
  ];

  return {
      tasks: cachedTasks || initialTasks,
      date: new Date()
  }
};

export default InitialState;
