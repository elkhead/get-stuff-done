const { app, BrowserWindow } = require("electron");

const path = require("path");
const isDev = require("electron-is-dev");

function createWindow() {
  let win = new BrowserWindow({
    minWidth: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
    },
    frame: true,
  });

  win.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );

  win.on('closed', () => win = null);

  // win.webContents.openDevTools()
}

app.whenReady().then(createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
